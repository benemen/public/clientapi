BeneClientAPI provides a local websocket server with a JSON-RPC 2.0 interface to workstation-level functions.

It provides access to underlying telephone software (Lync, Skype for Business or BeneDesk for Windows).

Full documentation is found at https://doc.beneservices.com/beneclientapi/.
